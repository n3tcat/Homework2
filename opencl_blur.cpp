
#include <stdio.h>
#include <stdlib.h>

#include <iostream>
#include <fstream>
#include <string>
using namespace std;

#ifdef __APPLE__
#define CL_USE_DEPRECATED_OPENCL_1_2_APIS
#include <OpenCL/opencl.h>
#else
#define CL_USE_DEPRECATED_OPENCL_1_2_APIS
#include <CL/cl.h>
#endif

#include <time.h>

const char* kernel_blur[] = {
	"__kernel void "
	"blur(__global int* input,"
	"		   __global int* output,"
	"		   int w, int h, int n)"
	"{"
	"	int i = get_global_id(0) + get_global_id(1)*h;"
	"	int l = n / 2;"
	"	int value = input[i];"
	"	int div = 1;"
	"	int ui = i - w;"
	"	int uic = 0;"
	"	while (ui >= 0 && uic < l)"
	"	{"
	"		value += input[ui];"
	"		div++;"
	"		ui -= w;"
	"		uic++;"
	"	}"
	""
	"	int di = i + w;"
	"	int dic = 0;"
	"	while (di < (w * h) && dic < l)"
	"	{"
	"		value += input[di];"
	"		div++;"
	"		di += w;"
	"		dic++;"
	"	}"
	""
	"	int li = i - 1;"
	"	int lic = 0;"
	"	int uli = li;"
	"	int ulic = l;"
	"	int dli = li;"
	"	int dlic = l;"
	"	while (li >= 0 && lic < l && ((li + 1) % w) != 0)"
	"	{"
	"		if (ulic > 0)"
	"		{"
	"			for (int k = 1; k < ulic; k++)"
	"			{"
	"				int q = li - (w*k);"
	"				if (q >= 0)"
	"				{"
	"					value += input[q];"
	"					div++;"
	"				}"
	"			}"
	"			ulic--;"
	"		}"
	"		if (dlic > 0)"
	"		{"
	"			for (int k = 1; k < dlic; k++)"
	"			{"
	"				int q = li + (w*k);"
	"				if (q < w * h)"
	"				{"
	"					value += input[q];"
	"					div++;"
	"				}"
	"			}"
	"			dlic--;"
	"		}"
	"		value += input[li];"
	"		div++;"
	"		li--;"
	"		lic++;"
	"	}"
	""
	"	int ri = i + 1;"
	"	int ric = 0;"
	"	int uri = ri;"
	"	int uric = l;"
	"	int dri = ri;"
	"	int dric = l;"
	"	while (ri < (w * h) && ric < l && ri % w != 0)"
	"	{"
	"		if (uric > 0)"
	"		{"
	"			for (int k = 1; k < uric; k++)"
	"			{"
	"				int q = ri - (w*k);"
	"				if (q >= 0)"
	"				{"
	"					value += input[q];"
	"					div++;"
	"				}"
	"			}"
	"			uric--;"
	"		}"
	"		if (dric > 0)"
	"		{"
	"			for (int k = 1; k < dric; k++)"
	"			{"
	"				int q = ri + (w*k);"
	"				if (q < w * h)"
	"				{"
	"					value += input[q];"
	"					div++;"
	"				}"
	"			}"
	"			dric--;"
	"		}"
	"		value += input[ri];"
	"		div++;"
	"		ri++;"
	"		ric++;"
	"	}"
	""
	"	output[i] = value / div;"
	"}"
};

int main(int argc, char** argv)
{
	int err;                            // error code returned from api calls

	cl_device_id device_id;             // compute device id 
	cl_context context;                 // compute context
	cl_command_queue commands;          // compute command queue
	cl_program program;                 // compute program
	cl_kernel kernel;                   // compute kernel			

	cl_uint dev_cnt = 0;
	clGetPlatformIDs(0, 0, &dev_cnt);

	cl_platform_id platform_ids[100];
	clGetPlatformIDs(dev_cnt, platform_ids, NULL);

	// Connect to a compute device
	int gpu = 1;
	err = clGetDeviceIDs(platform_ids[0], gpu ? CL_DEVICE_TYPE_GPU : CL_DEVICE_TYPE_CPU, 1, &device_id, NULL);
	if (err != CL_SUCCESS)
	{
		printf("Error: Failed to create a device group!\n");
		
		return EXIT_FAILURE;
	}

	// Create a compute context 
	context = clCreateContext(0, 1, &device_id, NULL, NULL, &err);
	if (!context)
	{
		printf("Error: Failed to create a compute context!\n");
		
		return EXIT_FAILURE;
	}

	// Create a command commands
	commands = clCreateCommandQueue(context, device_id, 0, &err);
	if (!commands)
	{
		printf("Error: Failed to create a command commands!\n");
		
		return EXIT_FAILURE;
	}

	program = clCreateProgramWithSource(context, 1, kernel_blur, NULL, &err);
	if (!program)
	{
		printf("Error: Failed to create compute program!\n");
		
		return EXIT_FAILURE;
	}

	// Build the program executable
	err = clBuildProgram(program, 0, NULL, NULL, NULL, NULL);
	if (err != CL_SUCCESS)
	{
		size_t len;
		char buffer[2048];
		printf("Error: Failed to build program executable!\n");
		clGetProgramBuildInfo(program, device_id, CL_PROGRAM_BUILD_LOG, sizeof(buffer), buffer, &len);
		printf("%s\n", buffer);
		
		exit(1);
	}

	// Create the compute kernel in the program we wish to run
	//
	kernel = clCreateKernel(program, "blur", &err);
	if (!kernel || err != CL_SUCCESS)
	{
		printf("Error: Failed to create compute kernel!\n");
		
		exit(1);
	}

	int w; int h;
	ifstream myfile;
	myfile.open(argv[1]);
	ofstream newfile;
	std::string name = argv[1];
	std::string ndim = argv[2];
	newfile.open("ocl-n" + ndim + "_" + name);
	string s;
	getline(myfile, s); // Id
	newfile << s << "\n";
	getline(myfile, s); // Version
	newfile << s << "\n";
	getline(myfile, s); // Dim
	newfile << s << "\n";

	string ws; string hs; bool done = false;
	for (int i = 0; i < s.length(); i++)
	{
		if (s[i] == ' ')
		{
			done = true;
			continue;
		}
		if (done)
		{
			hs += s[i];
		}
		else
		{
			ws += s[i];
		}
	}
	w = stoi(ws); h = stoi(hs);

	getline(myfile, s);
	newfile << s << "\n";
	int* img = new int[w*h];
	int k = 0;
	while (myfile.good())
	{
		if (k == w * h)
			break;
		getline(myfile, s);
		*(img + k) = stoi(s);
		k++;
	}
	myfile.close();

	int n = atoi(argv[2]);
	int l = (n / 2);

	int* img2 = new int[w * h];

	int img_size = sizeof(int) * (w*h);
	cl_mem img_input = clCreateBuffer(context, CL_MEM_READ_WRITE | CL_MEM_COPY_HOST_PTR, img_size, img, &err);
	cl_mem img_output = clCreateBuffer(context, CL_MEM_READ_WRITE, img_size, NULL, &err);

	err = clSetKernelArg(kernel, 0, sizeof(cl_mem), (void *)&img_input);
	err = clSetKernelArg(kernel, 1, sizeof(cl_mem), (void *)&img_output);
	err = clSetKernelArg(kernel, 2, sizeof(int), (void *)&w);
	err = clSetKernelArg(kernel, 3, sizeof(int), (void *)&h);
	err = clSetKernelArg(kernel, 4, sizeof(int), (void *)&n);

	//Launch OpenCL kernel
	size_t localWorkSize[2], globalWorkSize[2];

	if (err != CL_SUCCESS)
	{
		printf("Error: Failed to set kernel arguments! %d\n", err);
		
		exit(1);
	}

	localWorkSize[0] = 16;
	localWorkSize[1] = 16;
	globalWorkSize[0] = 1024;
	globalWorkSize[1] = 1024;

	clock_t tStart = clock();

	err = clEnqueueNDRangeKernel(commands, kernel, 2, NULL, globalWorkSize, localWorkSize, 0, NULL, NULL);

	if (err != CL_SUCCESS)
	{
		printf("Error: Failed to execute kernel! %d\n", err);
		
		exit(1);
	}

	//Retrieve result from device
	err = clEnqueueReadBuffer(commands, img_output, CL_TRUE, 0, img_size, img2, 0, NULL, NULL);

	if (err != CL_SUCCESS)
	{
		printf("Error: Failed to read output array! %d\n", err);
		
		exit(1);
	}

	cout << "Blur opencl con mask matrix di dimensione " << n << " su " << argv[1] << " completato in " << (double)(clock() - tStart) / CLOCKS_PER_SEC << "s." << endl;

	for (int i = 0; i < w * h; i++)
	{
		newfile << std::to_string(*(img2 + i)) << "\n";
	}

	//Shutdown and cleanup
	clReleaseProgram(program);
	clReleaseKernel(kernel);
	clReleaseCommandQueue(commands);
	clReleaseContext(context);

	free(img);
	free(img2);

	clReleaseMemObject(img_input);
	clReleaseMemObject(img_output);

	newfile.close();

	return 0;
}
